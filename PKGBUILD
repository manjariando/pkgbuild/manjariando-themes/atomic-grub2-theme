# Maintainer: tioguda <guda.flavio@gmail.com>

pkgname=atomic-grub2-theme
pkgver=4.0
pkgrel=12
pkgdesc="This theme contain minimal, modern and simple theme for your GRUB2 Loader."
arch=("any")
url="https://www.gnome-look.org/p/1200710"
license=('GPL3')
install=grub-theme.install
source=("Atomic-GRUB-Theme.tar.gz::https://github.com/lfelipe1501/Atomic-GRUB2-Theme/archive/master.zip"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/atomic"-{48,64,128}.png)
sha256sums=('0f482ccd5d14fcd3fd1657972079cfd0c96ac3b24d78c31b7758bff73d9be1a5'
            'dd9b19e32607ddc3f01f577e9afaeecba210baa9dd189038a1ee281e260a898e'
            'ef923fa11beca6067eb8a2f8c44b70283107def7eb6c38c39bb4440d1443596c'
            'd4505ba83d09a08534752d24d72930db845a30c24e29a79ba274724b5a0de93b'
            'd81e22f8480803e42cf17955001f56a508af71b54037441c7430a07ab7cb2ef8')

_atomic_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_atomic_desktop" | tee com.${pkgname//-/_}.desktop
}

package_atomic-grub2-theme() {
    depends=('grub')

    cd "${srcdir}/Atomic-GRUB2-Theme-master"
    install -dm755 ${pkgdir}/boot/grub/themes/Atomic
    cp -r Atomic ${pkgdir}/boot/grub/themes
    
    # Fix icon to dual boot with two versions of Manjaro
    cp -r ${pkgdir}/boot/grub/themes/Atomic/icons/manjaro.png ${pkgdir}/boot/grub/themes/Atomic/icons/manjarolinux.png

    # Appstream
    install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/atomic-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    sed -i "s:PACKAGE=.*:PACKAGE=Atomic:" "${startdir}/grub-theme.install"
}
